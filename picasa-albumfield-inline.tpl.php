<div class="picasa-list">
  <?php foreach ($items as $item) { ?>
    <div class="picasa-item">
      <a class="picasa-link fancybox" title="<?php print $item['title'] ?>" rel="album" href="<?php print $item['src'] ?>">
        <img class="picasa-img lazy" src="" data-src="<?php print $item['thumb'] ?>" title="<?php print $item['title'] ?>" alt="<?php print $item['desc'] ?>"/>
      </a>
    </div>
  <?php } ?>
</div>